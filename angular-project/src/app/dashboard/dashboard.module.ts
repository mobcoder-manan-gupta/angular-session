import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { AppModule } from '../app.module';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    SharedModule,
    AppModule,
    CommonModule,
    AppRoutingModule
  ]
})
export class DashboardModule { }
