import { ContentChildren, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent, } from './dashboard/home/home.component';
import { AboutComponent } from './dashboard/about/about.component';
 
const routes: Routes = [
    {path: '',   component: DashboardComponent,
        children:[{path: 'dashboard',   component: DashboardComponent},
                  {path: 'about',   component: AboutComponent},
                  {path: 'home',   component: HomeComponent},
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
