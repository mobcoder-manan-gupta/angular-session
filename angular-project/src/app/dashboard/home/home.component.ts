import { Component,OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public items:any;
  constructor(private serviceData:DashboardService){}
  ngOnInit(){
    this.items=this.serviceData.dummyData;
  }
  }

