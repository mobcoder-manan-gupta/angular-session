import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor() { }

  public dummyData=[
    {
      "name": "John Doe",
      "company": "ABC Corporation"
    },
    {
      "name": "Jane Smith",
      "company": "XYZ Corp"
    },
    {
      "name": "David Johnson",
      "company": "123 Industries"
    },
    {
      "name": "Emily Brown",
      "company": "Acme Co"
    },
    {
      "name": "Michael Davis",
      "company": "Sunset Enterprises"
    },
    {
      "name": "Sarah Wilson",
      "company": "Global Solutions"
    },
    {
      "name": "Robert Thompson",
      "company": "Tech Innovators"
    },
    {
      "name": "Jessica Lee",
      "company": "Swift Systems"
    },
    {
      "name": "Christopher Martin",
      "company": "Moonlight Labs"
    },
    {
      "name": "Amanda Clark",
      "company": "Starburst Industries"
    },
    {
      "name": "Daniel Rodriguez",
      "company": "Skyline Group"
    },
    {
      "name": "Olivia Wright",
      "company": "Prime Solutions"
    },
    {
      "name": "William Anderson",
      "company": "Dynamic Ventures"
    },
    {
      "name": "Sophia Martinez",
      "company": "Infinite Innovations"
    },
    {
      "name": "Matthew Taylor",
      "company": "Eagle Enterprises"
    },
    {
      "name": "Isabella Lewis",
      "company": "Silverline Systems"
    },
    {
      "name": "Joseph Hall",
      "company": "Pinnacle Technologies"
    },
    {
      "name": "Oliver Young",
      "company": "Spectrum Solutions"
    },
    {
      "name": "Emma Garcia",
      "company": "Phoenix Inc."
    },
    {
      "name": "Andrew Hernandez",
      "company": "Sunrise Software"
    },
    {
      "name": "Samantha Allen",
      "company": "Vista Corporation"
    },
    {
      "name": "Ethan King",
      "company": "Summit Industries"
    },
    {
      "name": "Mia Green",
      "company": "Apex Technologies"
    },
    {
      "name": "Alexander Lopez",
      "company": "Bluewave Systems"
    },
    {
      "name": "Abigail Hill",
      "company": "Peak Solutions"
    },
    {
      "name": "James Scott",
      "company": "Quantum Innovations"
    },
  ];  
}
